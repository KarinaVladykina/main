global _start
section .data
codes: db      '0123456789ABCDEF'
section .text
print_hex8:                     ;    rdi = char
  add rdi, codes
  mov rsi, rdi
  mov rax, 1
  mov rdi, 1
  mov rdx, 1
  syscall
  ret


_start:
  mov rdi, 12
  call print_hex8
  call exit



exit:
  mov rax, 60
  mov rdi, 0
  syscall
