../scripts/include-comments lec-3.org lec-3-notes.org
emacs lec-3-notes.org --batch -f org-html-export-to-html --kill
#pandoc -s --listings -o lec-3-notes-pandoc.tex lec-3-notes.org
#latexmk -xelatex -shell-escape -f lec-3-notes-pandoc.tex
#pandoc --listings --pdf-engine=xelatex --pdf-engine-opt=-shell-escape -o lec-3-notes-pandoc.pdf lec-3-notes.org
# pandoc -F pandoc-minted.py --pdf-engine=xelatex --pdf-engine-opt=-shell-escape -o lec-3-notes-pandoc.tex lec-3-notes.org
